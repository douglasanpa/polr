<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Link;

class TestURLLong extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testa todas as URLs para saber se continuam válidas';

    /**
     * Execute the console command for Laravel 5.5 and newer.
     *
     * @return void
     */
    public function handle()
    {
        $this->fire();
    }

    public function get_http_status($url){ 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HEADER, TRUE); 
        curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
        curl_close($ch); 
        return $httpCode; 
    } 

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $links = Link::where('is_disabled',0)->get();
        $status = [];
        foreach($links as $link){
            $status[$link->short_url]['short_url'] = $link->short_url;
            $status[$link->short_url]['long_url'] = $link->long_url;
            $status[$link->short_url]['status'] = $this->get_http_status($link->long_url);
            if (PHP_SAPI === 'cli'){
                $this->comment("A URL {$link->short_url} retornou o status ".$status[$link->short_url]['status']);
            }
        }
        return $status;

    }
}
