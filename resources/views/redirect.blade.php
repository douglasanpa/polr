@extends('layouts.base')
<meta http-equiv="Refresh" content="10; url={{$long_url}}">
@section('css')
<link rel='stylesheet' href='/css/shorten_result.css' />
@endsection

@section('content')
<h3>Redirecting to</h3>

<a href='{{$long_url}}' class='btn btn-info'>{{$long_url}}</a>

@endsection


@section('js')
<script src='/js/qrcode.min.js'></script>
<script src='/js/clipboard.min.js'></script>
<script src='/js/shorten_result.js'></script>
@endsection
